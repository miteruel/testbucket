import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Home2meComponent } from '../views/home2me/index';

import { LoginComponent } from '../views/login/index';
import { SignupComponent } from '../views/signup/index';
import { MainappComponent } from '../views/mainapp/index';



import { AuthGuard } from '../_guards/index';
import { AfterlogComponent } from '../views/afterlog/afterlog.component';

const appRoutes: Routes = [
  // { path: '', component: HomeComponent },
// { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    // { path: '', component: app },
    { path: 'ho2me', component: Home2meComponent },
     { path: 'login', component: LoginComponent },


  { path: 'home', component: MainappComponent , canActivate: [AuthGuard]},

  //  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: 'ho2me' }
];

export const routing = RouterModule.forRoot(appRoutes);

@NgModule({
  exports: [
    RouterModule
  ]
})
export class B2meRoutingModule { }
