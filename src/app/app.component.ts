
import { Component, ViewChild } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { AgGridNg2 } from 'ag-grid-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //  @ViewChild('agGrid') agGrid: AgGridNg2;


  title = 'app';

  private defaultColDef;

  private gridApi;
  private gridColumnApi;
  private rowData: any[];



  private pinnedTopRowData;
  private pinnedBottomRowData;





  columnDefs = [
    { headerName: 'ID', field: 'ID' },
    { headerName: 'Title', field: 'Title' },
    { headerName: 'Author', field: 'Author' },
    { headerName: 'Abstract', field: 'Abstract' }
  ];




  constructor(private http: HttpClient) {
    //colde
    this.defaultColDef = {
      width: 150,
      editable: true,
      filter: "agTextColumnFilter"
    };
    this.pinnedTopRowData = this.getPinnedTopData();
    this.pinnedBottomRowData = this.getPinnedBottomData();
    this.defaultColDef = { editable: true };

    this.columnDefs.forEach(function (data) {

      //data.headerNamefield.editable= true
    }
    );
  }


  onBtStopEditing() {
    console.log('onBtStopEditing')
    this.gridApi.stopEditing();
  }
  
  onBtStartEditing(key, char, pinned) {
    console.log('onBtStartEditing')
    this.gridApi.setFocusedCell(0, "Title", pinned);
    this.gridApi.startEditingCell({
      rowIndex: 0,
      colKey: "ID",
      rowPinned: pinned,
      keyPress: key,
      charPress: char
    });
  }
  
  onBtNextCell() {
    console.log('onBtNextCell')
    this.gridApi.tabToNextCell();
  }
  
  onBtPreviousCell() {
    console.log('onBtPreviousCell')
    this.gridApi.tabToPreviousCell();
  }
  
  onBtWhich() {
    console.log('onBtWhich')
    let cellDefs = this.gridApi.getEditingCells();
    if (cellDefs.length > 0) {
      var cellDef = cellDefs[0];
      console.log(
        "editing cell is: row = " +
        cellDef.rowIndex +
        ", col = " +
        cellDef.column.getId() +
        ", floating = " +
        cellDef.floating
      );
    } else {
      console.log("no cells are editing");
    }
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  
    params.api.sizeColumnsToFit();
  }
  
  
  getPinnedTopData() {
    return [
      {
        firstName: "##",
        lastName: "##",
        gender: "##",
        address: "##",
        mood: "##",
        country: "##"
      }
    ];
  }

  getPinnedBottomData() {
    return [
      {
        firstName: "##",
        lastName: "##",
        gender: "##",
        address: "##",
        mood: "##",
        country: "##"
      }
    ];
  }
  
  getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === "undefined" ? event.keyCode : event.which;
  }
  
  isCharNumeric(charStr) {
    return !!/\d/.test(charStr);
  }
  
  isKeyPressedNumeric(event) {
    var charCode = this.getCharCodeFromEvent(event);
    var charStr = String.fromCharCode(charCode);
    return this.isCharNumeric(charStr);
  }

  ngOnInit() {
    //this.rowData = 
    this.http.get('http://127.0.0.1:8092/blog/Article/blobs').
      subscribe(customers => {
        console.log('result is ', customers);
        console.log(customers);
        const s: any = customers;
        const res: any = s;// s.values;
        if (res) {


          this.rowData = res

        }
        //customersService.



        console.log(this.rowData);
      }, error => {
        console.log('error http is ', error);
      });


  }
}






