import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http'; // +
import {Usuario} from '../modelos/usuario';
import {Observable, of} from 'rxjs';
import {AppComponent} from '../app.component';

import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  public rota = 'http://localhost:8092/blog';

  constructor(private http: HttpClient) {
    console.log('UsuariosService ', http);
  }

  getAll() {
    return this.http.get<Usuario[]>('/api/users');
  }

  getById(id: number) {
    return this.http.get('/api/users/' + id);
  }

  miUrl(mas: string) {
    return this.rota + mas;
  }

  create(user: Usuario) {
    console.log(user);
    return this.http.post(this.miUrl('/User/Signon')
      ,
      {
        username: user.username,
        password: user.password
      });


  }

  update(user: Usuario) {
    return this.http.put('/api/users/' + user.id, user);
  }

  delete(id: number) {
    return this.http.delete('/api/users/' + id);
  }

  validateLogin(user: Usuario) {
    return this.http.post(this.miUrl('/User/Login')
      ,
      {
        // id : user.id,
        username: user.username,
        password: user.password
      });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }
}
