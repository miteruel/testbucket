import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; // +
import { SampleRecord } from '../modelos/SampleRecord';
import { Observable, of } from 'rxjs';
import { AppComponent } from '../app.component';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SampleRecordService {
    constructor(private http: HttpClient) {
    console.log('UsuariosService ', http);
     }

    getAll() {
        return this.http.get<SampleRecord[]>('/api/users');
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id);
    }

    create(user: SampleRecord) {
        return this.http.post('/api/users', user);
    }

    update(user: SampleRecord) {
        return this.http.put('/api/SampleRecord/' + user.ID, user);
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id);
    }

   validateLogin(user: SampleRecord) {
      console.log('validateLogin http://192.168.1.131:8080/api/user/login');

      const langPath = '/assets/mocklogin.json';
     return this.http.get('http://127.0.0.1:8080/demo/ajax.html', {
    });
   }

  login (username: string, password: string) {
      return this.http.post<SampleRecord>('http://192.168.1.131/api/authenticate', { username: username, password: password });
      //  return this.http.post<Usuario>('/api/authenticate', { username: username, password: password });
         /*   .map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });*/
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
