import {Component, OnInit} from '@angular/core';
import {UsuariosService} from '../../servicios/usuarios.service';
import {Usuario, mainUser} from '../../modelos/usuario';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  modelo: Usuario = mainUser;
  loading = false;
  returnUrl: string;
  error: string;

  signup() {

    console.log('signup');
    // mock 1. Si password es igual que username da por bueno el login
    if (this.modelo.IsAnonimo()) {
      this.modelo.DoLogedAnonimo();
    }
    if (this.modelo.username && this.modelo.password) {
      // console.log('validateLogin');
      this.loginService.create(this.modelo).subscribe(result => {
      const s: any = result;
      const res: any = s.Result;
        if (res) {
          console.log(res);
          const id: string = res.ids;

          if (id) {


              // console.log('si def ', id);
//              this.modelo.DoLoged(id);
              this.router.navigate(['login']);

              // this.modelo.username = 'logeado'

          } else  {
            const e: string = res.error;
            if (e) {
              this.error = e;
              this.modelo.username = '';
            }

          }
        }

        console.log('result is ', result);

      }, error => {
        console.log('error http is ', error);
      });
    } else {
      alert('enter user name and password');
    }

  }

  constructor(
     private route: ActivatedRoute,
     private router: Router,
    public loginService: UsuariosService) {
    console.log('signup ', loginService);

  }


  ngOnInit() {
  }


}
