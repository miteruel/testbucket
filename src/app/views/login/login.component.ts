import {Component, OnInit} from '@angular/core';
import {Usuario, mainUser} from '../../modelos/usuario';
import {UsuariosService} from '../../servicios/usuarios.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  modelo: Usuario = mainUser;
  loading = false;
  returnUrl: string;

  login() {
    console.log('login');
    // mock 1. Si password es igual que username da por bueno el login
    if (this.modelo.IsAnonimo()) {
      this.modelo.DoLogedAnonimo();
      this.router.navigate(['home']);
      return;
    }
    this.loginService.validateLogin(this.modelo).subscribe(result => {
      console.log('result is ', result);
      const s: any = result;
      const res: any = s.Result;
      if (res) {
        const id: string = res.ids;
        if (id) {
          this.modelo.DoLoged(id);
          this.router.navigate(['home']);
        } else {
          if (res.error) {
            this.modelo.errors = res.error;
          }
        }
      }
    }, error => {
      console.log('error http is ', error);
    });


  }


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public loginService: UsuariosService) {
    console.log('login ', loginService);

  }

  ngOnInit() {
  }

}
