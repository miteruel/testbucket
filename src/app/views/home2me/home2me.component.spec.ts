import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Home2meComponent } from './home2me.component';

describe('Home2meComponent', () => {
  let component: Home2meComponent;
  let fixture: ComponentFixture<Home2meComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Home2meComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Home2meComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
