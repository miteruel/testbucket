# mORMot&Angular


Little example of an SPA Angular typeScript client which connect with a Delphi mORMot secure server.
The client and server must implement secure connection using REST "Query Authentication".
[mORMot Authentication-and-Authorization](http://blog.synopse.info/post/2013/06/07/Authentication-and-Authorization)

The client APP can be serve by same data REST server or a different web server (or even local file) 
that means that REST server must accept CORS connections, and client APP can be easy converted in Native or Desktop APP.    
 
## Contents ##

- [Angular-client](#Angular-client)

- [Delphi-server](#Delphi-server)

- [Uses](#uses)
		  - [Download And Install](#download-and-install)
		  - [Development Server](#development-server) 
- [Changes](#changes) 
		  - [Version 0.2](#version-0.2)

## Angular-client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.
I used some code of [mORMot-Typescript-client](https://github.com/esmondb/mORMot-Typescript-client),
but I do a little reFactor putting the SHA/CRC functions in different unit (for reuse) and changing the other code to use httpClient class, and removing  obsolete functions (for this project).
 
It must implement the following features:

1. /login : Login  
2. /signup : Register Step 1 basic data
3. /main : The main window when application is logged
4. /CRUD_TEMPLATE: Complete example of CRUD operation for a table.   
	* Implement CRUD services functions. Adding the Authorization hash in each REST call.
	* Implement Dynamic Form to edit a register of a Model
	* Implement DataGRID edition of REST Tables [ag-grid](https://www.ag-grid.com/angular-getting-started/) 
	      

Is SPA (single page application), the HTML page is loaded only one time. 
Then you can navigate the APP using angular router and refresh data using REST.      
The page have the user session in the memory until application was reloaded, 
and use this session info to create the REST "Authentication" calls. 

Other features.
* Include multi.lingual support


## Delphi-server

For server, I use [mORMot\SQLite3\Samples\30 - MVC Server] example, extending the functionally (without touch any unit), to
implement REST "Query Authentication". 

Really is not a Big problem to secure the server.

Include TSQLAuthGroup,TSQLUser in Model   

~~~pas    
function CreateSecureModel: TSQLModel;
begin
  result := TSQLModel.Create([TSQLAuthGroup,TSQLUser,TSQLBlogInfo,TSQLAuthor,
    TSQLTag,TSQLArticle,TSQLComment,TSQLArticleSearch],'blog');
  ...
end;
~~~

and create the rest server allowing Authentication

~~~pas
TSQLRestServerDB.Create(aModel, aDBFileName_, true);
~~~


## 1. /login 
 
## 2. /signup 

If you are not logged in the server you can't do any operation. 
So, how can I do a signup in a secure REST server if i'm not logged.
Maybe there is a better solution but I use a simple non Athentificate REST Interface which only implement
a fiew signup operations and other Mock funcions. 
The signup process can be complex in real systems.
We have a very simple Rest call invoked with user/password  params, and return the ID of the new User if can be created, or error msg if can't be created.
Password will be hashed in SHA 
  

## Uses ##

## Download And Install
	
	Download this repository.  Suppose you put it in a folder called 'Fromb2me4u'

~~~
cd Fromb2me4u
npm install
~~~


## Development Server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Documentation 		

* Play help server: [compodoc](https://compodoc.app/guides/usage.html)
~~~
compodoc -s
~~~ 		 

* Update help files:
~~~
compodoc -p tsconfig.json -n 'bit2ne4u'
~~~ 		 
 		

## Create a new Angular project.

Using angular-cli
 
~~~
ng new fromb2me4u
cd fromb2me4u
ng serve --open
~~~


## Changes ##

## Version 0.1

* Initial public version
      
  

  
