# Fromb2me4u

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

Little front-end with Angular framework. It must implement m at least 3 views. 

1. /login : Login  
2. /login2fa : Popup 2FA after login
3. /signup : Register Step 1 basic data 

## Contents ##

- [Changes](#changes) 
		  - [Version 0.2](#version-0.2)
- [Uses](#uses)
		  - [Download And Install](#download-and-install)
		  - [Development Server](#development-server) 
  
## Changes ##

## Version 0.2

* ReFactor the folders  and clean the source 
* ReFactor the Usuario.service to work with external Mock CORS server. 
* Include a binary server which run in 8092 port to try and mock the login and signup users
  

## Uses ##

## Download And Install
	
	Download this repository.  Suppose you put it in a folder called 'Fromb2me4u'

~~~
cd Fromb2me4u
npm install
~~~


## Development Server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##  Other Requirements

* locale support for English and Spanish.
* Separate Components in files and folders .
* Automation tasks for compiling and deployment.
* Mocking Ajax calls. And login strategies 
* Commits in bitbucket.

## Spanish 

Desarrollo de un pequeño frontend de 3 vistas con el framework Angular.
Al menos tiene que implementar las siguientes pantallas:

1. Login - Ruta: /login (es la que carga nada más arrancar)
2. Popup 2FA: Ruta /login-2fa (carga tras pulsar login)
3. Registro Step 1 Datos básicos: Ruta: /signup

TODO:

* Mock de las llamadas Ajax
* Tarea de automatización para compilado, minimizado, concatenado y lo que se considere.
* Organizar los componentes en carpetas separando css, ts, html (si se desea pueden usarse lenguajes como scss, yaml,...)
* Rutas.
* Soporte multi idioma: español e inglés (puede ser inglés de Google Translator sin problemas, no importa la calidad de la traducción). Añadir un selector de idioma.
* Estrategia para gestión de logs (sin desarrollar cualquier parte del backend que pueda requerir).
* Commits en repositorio git (puede usarse bitbucket)

* Ideas sobre extras que añadir:
* Ser ejecutado a través de un docker con una imagen linux.
* Batería de tests.

*añadir documento con instrucciones de despliegue, cuestiones a tener en cuenta y 
futuras mejoras / aproximaciones que hicieran del código un código mejor con algo más de tiempo.




## Hoja de ruta
* Actualizar herramientas
    * Actualiza npm y node.js
    * Actualiza angular/cli
~~~
		npm install -g @angular/cli
~~~
* Crear proyecto
~~~
ng new fromb2me4u
cd fromb2me4u
ng serve --open
~~~
* Generar Modelo de datos
	* Crea archivo modelos/usuario.ts
	* Modifica modelo 
~~~
 	ng generate class modelos/usuario
~~~
	
* Generar rutas
~~~
ng generate module b2me-routing --module=app
~~~

* Generar vista Login

	* Crear componente /login
	* añadir a las rutas b2me-routing , 
		de momento sirve para probar las rutas.
~~~
		ng generate component login
~~~
	
		
* Generar servicio de usuarios

	* usar mocks para afinar login y usuarios
~~~
ng generate service servicios/usuarios
~~~

* Generar vista signup

~~~
	ng generate component signup
~~~
	

* Generar vista popup
~~~	
	ng generate component popupLogin
~~~	

* Crear vista de aplicación cuando esta logeada
~~~
 ng generate component views/Mainapp
~~~
 
	
* Login vs restricción acceso 


## Memoria de proyecto

Este repositorio es un ejemplo muy simple de uso personal. 
Lo he hecho con la última versión de angular y usando typescript ya que tenía poca experiencia con ellos y me sirve de aprendizaje también.

Lo primero he actualizado las herramientas npm, node y he instalado angular-cli.

También he instalado Angular IDE, que es un derivado de eclipse y parece que funciona bastante bien .
[Angular IDE](https://www.genuitec.com/products/angular-ide/)

Solo lo voy a usar como editor. Aunque tiene algunas herramientas para automatizar, usaré directamente los comandos de angular-cli para crear las carpetas y componentes.


El proyecto es un ejemplo de uso de angular para hacer registro y login  a una aplicación web genérica.
En un principio no voy a usar encriptación en los datos ya que se trata de un primer ejemplo. 
Tampoco incído en la parte del servidor, ya que deberia ser un servidor “generico”

## Crear la infraestructura del proyecto.

La forma más sencilla y completa de crear un proyecto angular es usar angular-cli.
Crea todos los archivos necesarios para un proyecto completo. Incluidas las scripts para probar y compilar el proyecto, depurarlo,
hacer test, ...
 
> Esto genera un proyecto completo angular
~~~
ng new fromb2me4u
cd fromb2me4u
ng serve --open
~~~

## Generar el modelo de datos.
	
Lo primero es generar un modelo de los datos del usuario del registro y login.

~~~
	ng generate class modelos/usuario
~~~

[/app/modelos/usuario.ts](https://bitbucket.org/miteruel/testbucket/src/master/src/app/modelos/usuario.ts)

```typescript
export class Usuario {
  constructor(
    public id: number,
    public username: string,
    public password: string,
    public firstName: string,
    public lastName?: string) {}
}

export const mainUser: Usuario = new Usuario(1, 'bit2me', 'bit2me', 'test purpose');
```

Aunque choca un poco los parámetros de los constructores (   public id: number,) 
indican que el parámetro es a la vez un miembro (publico o privado) de la clase.
Esto ofrece una mínima sintaxis para resolver la construcción de objetos

## Crear componente /login
~~~
ng generate component login
~~~

## Crear componente /registro
~~~
ng generate component registro
~~~

## Soporte multi idioma: 
El problema de la internacionalización en Angular tiene al menos 2 formas diferentes de enfocarlo.

## Crear diferentes versiones del mismo programa en diferentes idiomas.
básicamente consiste en compilar los mismos fuentes con diferente configuración.[i18n](https://angular.io/guide/i18n).
**No es apropiada para este ejemplo**

~~~
ng xi18n
~~~

## Versión del programa que use el idioma elegido en runtime.
	
Esta opción se puede implementar de varias maneras también, pero consiste básicamente en tener un diccionario con los textos traducidos a diferentes idiomas.
 
También podría implementarse una traducción automatica a través de algún servicio en línea, pero está fuera de contexto si son pocos fijos los textos a dar soporte.

De las formas que hay la más elegante es usar “pipes”. Permite una forma muy sencilla de “decorar” textos.
>Esto visualiza el contenido de la variable dataVar en letras mayusculas.
~~~
{{ dataVar | uppercase }}
~~~

>La intención es crear una “pipe” para traducir el texto.
~~~
	{{ 'TITLE' | translate }}
~~~

Primero crearemos un servicio que tenga los diccionarios de texto al lenguaje requerido.

Este servicio tomará los datos de ficheros JSON que estan el la carpeta assets/i18 . 
Inicialmente  estarán es.json y en.json que son los diccionarios para español e inglés 
```
{
  "TITLE": "Mi Aplicacion i18n (es)",
  "User Name": "Nombre Usuario"
}
```

>TODO:: Completar diccionario

~~~
ng generate service servicios/translate
~~~

Para leer archivos JSON se puede usar el comando http.get , creando una “Promesa”. Nos introduce en nuevas características del lenguaje TypeScript. Observables. Consiste en un mecanismo robusto para manejar llamadas asíncronas.
>/app/servicios/translate.service.ts
```
use(lang: string): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      const langPath = `assets/i18n/${lang || 'en'}.json`;
      this.http.get<{}>(langPath).subscribe(
        translation => {
          this.data = Object.assign({}, translation || {});
          resolve(this.data);
        },
        error => {
          this.data = {};
          resolve(this.data);
        }
      );
    });
```

El servicio podrá cargar el diccionario que se seleccione a través de:
~~~
translate.use(‘es’)
~~~


Ahora podemos  crear el objeto pipe translate
~~~
ng g pipe pipes/translate --module=app
~~~


[/app/pipes/translate.pipe.ts](./src/master/src/app/pipes/translate.pipe.ts)

```typescript

export class TranslatePipe implements PipeTransform {
  constructor(private translate: TranslateService) {}  // * 
    transform(value: any, args?: any): any {
      return this.translate.data[value] || value; // *
    }
}
```

Cambios en app.component.ts para iniciar el dicionario por defecto.
[/app/app.component.ts](./src/master/src/app/app.component.ts)

```typescript

 constructor(private translate: TranslateService) {
    translate.use('es').then(() => {
      console.log(translate.data);
    });
  }
  setLang(lang: string) {
      this.translate.use(lang);
    }
```


 
 ## Login SignUp
 
El problema de hacer un sistema seguro de Logins es antiguo y puede ser tan complejo como queramos hacerlo.
Un sistema suficiemente seguro no depende solo del Front-End, ya que la parte principal de control debe residir en el propio servidor.
Tiene que validar si el usuario esta identificado o no. 

La aplicación cliente (en este caso la pagina web)  tiene que llevar su propio control para saber cuando esta logueada o no. 
Especialmente en las SPA (single page application) es importante que lleven ese control para evitar derivar al servidor todo el control visual.
 
Para poder acceder a un recurso publico, normalmente se pregunta el usuario y la contraseña. 
Es una medida basica de seguridad que cualquier aplicación debe tener. 
Además otras partes necesitan "algo" por no decir "mucho" de como esta hecho este componente especial.
Se supone que un usuario puede acceder o no segun que cosas dependiendo de su perfil de usuario y 
nunca antes de haberse identificado adecuadamente. 		   
Aunque los sistemas más seguros utilizan otras comprobaciones, como comprobar IP's, proxys, tokens, encriptación, etc...
 
No es la intención hacer el sistema más seguro, sino el más generico y como base para una aplicación más especializada.
 
Puntos a tratar:

 * Crear un sistema basico para registrarse en un servidor.
 * La pagina web tiene que saber cuando está o no logueada para saber que vistas tiene que fabricar. 
 * Si no esta logueada tiene que llevarnos a la pantalla login.
 * De la pantalla login se accede a la aplicación principal o puede acceder al registro de un nuevo usuario.
 * El registro de un nuevo usuario se hace en varios pasos. 
 		1. El primer paso pide el correo electronico y una contraseña.
 		2. El segundo paso se espera la confirmación del alta.
 		3. Se termina de rellenar la información del usuario.
 		
 		   
## Documentación 		

* Ejecutar servidor de ayuda:
~~~
compodoc -s
~~~ 		 

* Actualizar / generar ficheros de ayuda:
~~~
compodoc -p tsconfig.json -n 'bit2ne4u'
~~~ 		 
 		
* [compodoc](https://compodoc.app/guides/usage.html)
 		
 		
 


https://www.ag-grid.com/angular-getting-started/
https://angular-2-training-book.rangle.io/v/v2.3/handout/http/catching-rejections/



https://bitbucket.org/miteruel/testbucket/src/master/src/app/modelos/usuario.ts



https://bitbucket.org/miteruel/testbucket/src/master/src/app/modelos/usuario.tshttps://bitbucket.org/miteruel/testbucket/src/master/src/app/modelos/usuario.ts


~~~
ng generate service servicios/articulos
~~~
